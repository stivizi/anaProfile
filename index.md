+++
Title = "BioPilia Living Design"
+++

# *The Ecology Of Woman As Art*
***We Are Our Ancestors Wildest Dreams.***
***A Unique Opportunity For The Creation Of Something New And Beautiful.***

## *Design Through Spirit.*
**It's time to step out from the story of our present situation, and write a New Future. A new story about creating healing spaces and connecting Soil to Soul, the Symbiotic Real.**

*The relationship between human and environment is highlighted, not human and 'things'.*

#### **Mission: Personal empowerment through ecological literacy, to awaken possibility.**
**Knowledge to design a more desirable, fulfilling, life supporting; Ecologically Modern and Realistic Lifestyle.**

#### *Vision: Supporting Women Creating A Legacy With Mother Earth.*
**Ones Dream And The Dream Of Mother Earth As One.**

## *Ana ~ Private Consulting / Live Online 'Salon Style' Experiences / Public Speaking*
### **Valuing life, relationships, experiences, and soul-care.**
**Discovering and sharing solutions to everyday environMental impacts that we inflict on ourselves and our surroundings.**

**Creating healthy living environments, turning the tables from an environMental withdraw to a environMental deposit for the future.**

**With a diverse spectrum of Design and Ecological Health related knowledge from 25+ years of Self-initiated Fieldwork Experience, designing, launching and running new ideas.**

**Privately collaborating with individuals and small groups. Designing and hosting unique meaningful ecological experiences since 1994, Eco-Built Living spaces since 2004, public speaking since 2009.**

#### *Vivarium Online ~ Live Hosted Salon Style Experiences*
***Strengthening Relationships and commUnity by revitalizing 'The Art Of Conversation'.***

**To awaken possibility, refine the taste and increase the knowledge of the participants through guided conversation organized around cultures and ideas reflective of the era in which we live.**

#### Designed To Be Fun, Creative, Interactive and Resourceful.
**A safe space for WoMen to explore, learn, reflect; on how we interact, impact and live in our personal environments; homes, work, travel, ..., spaces.**

**A Self-creative Exploration to insight deep and meaningful change by working with the individuals natural enthusiasm.**

**An atmosphere where discussions can flourish, a space in which participants can test creative projects and ideas in the process of self-cultivation.**

#### *The way out is personal responsibility.*
**Curated to attract those individuals who are genuine in their effort to reach their own potential and help others do so in the process.** Custom Salon Experiences Available.

#### CommUnity Support.
**Come together in common Unity "Online Global CommUnity for Local Impact".**

**Private 'Vivarium Womens Salon'; invite only, please inquire.**

#### Explore ~ Connect

[**Public Profile & Reviews**](https://www.airbnb.com/users/show/3774308)

**[YouTube](https://www.youtube.com/channel/UCJsiffL5swNOi37qDOsk8ZQ/videos)**

**Instagram - @vivarium_place_of_life**

[**Chat with me directly**](https://matrix.to/#/!ibYXXCkubbZiWtkmhX:matrix.org?via=matrix.org)

#### [*Payments with Paypal*](https://www.paypal.me/vivarium)

